#FBEB73 minecraft:allium
#FA75A8 minecraft:andesite
#D16C77 minecraft:andesite_slab
#F84007 minecraft:andesite_stairs
#189B99 minecraft:barrel
#3DE8FE minecraft:birch_leaves
#A8E5E5 mekanism:block_salt
#8AE729 minecraft:brain_coral_block
#207534 minecraft:brown_mushroom
#642081 minecraft:brown_terracotta
#B78C11 minecraft:brown_wool
#BA9FBF minecraft:bubble_coral_block
#5C47C2 biomesoplenty:bush
#1BD05D minecraft:carved_pumpkin
#87E47E appliedenergistics2:charged_quartz_ore
#3FD900 mysticalworld:charred_log
#F522F4 minecraft:chest
#AA9109 minecraft:chiseled_stone_bricks
#7FDE54 minecraft:clay
#DE242D minecraft:coal_ore
#FE4F0A minecraft:coarse_dirt
#058353 minecraft:cobblestone
#7331AC minecraft:cobblestone_slab
#3D8BFC minecraft:cobblestone_stairs
#8B8A11 minecraft:cobweb
#6BB940 create:copper_ore
#179BD9 mekanism:copper_ore
#54193C mysticalworld:copper_ore
#809605 thermal:copper_ore
#306005 minecraft:cracked_stone_bricks
#59E2AB quark:crimson_chest
#E9A8F5 minecraft:crying_obsidian
#B2A2E1 minecraft:dandelion
#E3DF1E minecraft:dark_oak_leaves
#3C8240 minecraft:dead_brain_coral
#23FC0E minecraft:dead_brain_coral_block
#934E14 minecraft:dead_brain_coral_fan
#8344C3 minecraft:dead_brain_coral_wall_fan
#8023B7 biomesoplenty:dead_branch
#0CAEAC minecraft:dead_bubble_coral
#44277A minecraft:dead_bubble_coral_block
#5F6480 minecraft:dead_bubble_coral_fan
#2051D7 minecraft:dead_bubble_coral_wall_fan
#2D68D6 minecraft:dead_bush
#A34B96 minecraft:dead_fire_coral
#E7ECE4 minecraft:dead_fire_coral_block
#297C6A minecraft:dead_fire_coral_fan
#5A9C2D minecraft:dead_fire_coral_wall_fan
#E8DD23 minecraft:dead_horn_coral
#CB60B1 minecraft:dead_horn_coral_block
#3B7940 minecraft:dead_horn_coral_wall_fan
#D986B1 biomesoplenty:dead_leaves
#C9E164 minecraft:dead_tube_coral
#4E7032 minecraft:dead_tube_coral_block
#547B38 minecraft:dead_tube_coral_fan
#E4AE1F minecraft:dead_tube_coral_wall_fan
#7F8537 quark:deepslate
#BED665 minecraft:diorite
#4CF9DA minecraft:diorite_slab
#C600AA minecraft:diorite_stairs
#80497A minecraft:dirt
#2882C6 subwild:dirt_patch
#596328 subwild:dirt_slab
#8E1338 subwild:dirt_stairs
#B666A8 minecraft:dispenser
#B26FDF create:dolomite
#009B14 minecraft:farmland
#B9B269 minecraft:fire_coral_block
#9698DB create:gabbro
#060B35 minecraft:granite
#56470A minecraft:granite_slab
#B0C1DA minecraft:granite_stairs
#3F84B7 minecraft:grass_block
#91669F simpletomb:grave_normal
#88BAFA minecraft:gravel
#75D446 subwild:gravel_patch
#759A9D iceandfire:graveyard_soil
#9D2636 minecraft:horn_coral_block
#0575BD minecraft:infested_stone
#D7EADE minecraft:iron_ore
#7E0D49 minecraft:jack_o_lantern
#228015 minecraft:ladder
#83CED3 minecraft:lava
#72D77D minecraft:lever
#CBF1B4 minecraft:light_blue_terracotta
#D4FF84 minecraft:light_blue_wool
#7482E4 minecraft:lilac
#746434 create:limestone
#9812BA quark:limestone
#DE9468 minecraft:magma_block
#C8A620 biomesoplenty:maple_leaves
#FEA047 quark:marble
#E696F3 subwild:mossy_andesite
#A4FC4B minecraft:mossy_cobblestone
#39C2A0 subwild:mossy_diorite
#C2AEC5 subwild:mossy_dirt
#80F770 subwild:mossy_granite
#F7BE45 subwild:mossy_gravel
#EFFE93 subwild:mossy_stone
#2F738D minecraft:mossy_stone_brick_slab
#483B67 minecraft:mossy_stone_bricks
#454322 minecraft:netherrack
#73753E minecraft:oak_fence
#AD9EB5 minecraft:oak_leaves
#BBA6D1 minecraft:oak_log
#E3886A minecraft:oak_planks
#374FD3 minecraft:oak_slab
#72BC63 minecraft:oak_stairs
#855CF4 minecraft:oak_trapdoor
#392E46 minecraft:oak_wood
#4A5E4E minecraft:obsidian
#71546E biomesoplenty:orange_autumn_leaves
#BF0745 immersiveengineering:ore_aluminum
#0F3A9E immersiveengineering:ore_copper
#067C92 immersiveengineering:ore_silver
#B29458 mekanism:osmium_ore
#F6678E minecraft:oxeye_daisy
#AB1C58 minecraft:peony
#BB7F65 minecraft:podzol
#C05C71 subwild:podzol_patch
#6A5197 minecraft:polished_andesite
#44E938 minecraft:polished_andesite_stairs
#AFAC0D minecraft:poppy
#40BB61 minecraft:prismarine
#5ED25E minecraft:prismarine_slab
#C3C82E minecraft:prismarine_stairs
#E6B35B minecraft:pumpkin
#DA294B minecraft:purple_wool
#F0410F appliedenergistics2:quartz_ore
#FB9751 minecraft:quartz_slab
#025439 minecraft:red_mushroom
#4A5246 minecraft:red_terracotta
#83B596 minecraft:red_wool
#A7ADEC quark:root
#9C3ECB minecraft:rose_bush
#86FC77 minecraft:sand
#7760DE minecraft:skeleton_skull
#CB82F8 minecraft:smooth_stone_slab
#13F5AC minecraft:spruce_leaves
#49DF61 minecraft:spruce_planks
#64F88A minecraft:spruce_slab
#D9135A minecraft:spruce_stairs
#DC3458 minecraft:stone
#B21185 minecraft:stone_brick_slab
#43D995 minecraft:stone_brick_stairs
#F8955F minecraft:stone_bricks
#AB5BC7 minecraft:stone_slab
#139B57 minecraft:stone_stairs
#329213 minecraft:sweet_berry_bush
#B50B59 mekanism:tin_ore
#F188BC mysticalworld:tin_ore
#1E0585 thermal:tin_ore
#2035B7 minecraft:tube_coral_block
#25F8B7 mekanism:uranium_ore
#88779D minecraft:vine
#A613A0 mana-and-artifice:vinteum_ore
#5090DC subwild:water_puddle
#1C0619 waystones:waystone
#7D85A4 subwild:wet_andesite
#E8EE9F subwild:wet_granite
#F9F582 subwild:wet_stone
#BC20FE minecraft:white_wool
#0A520E minecraft:wither_rose
#0A2034 biomesoplenty:yellow_autumn_leaves
#D9C16D quark:yellow_blossom_leaves
#6137CB create:zinc_ore
